package com.qmcy.demux;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.qmcy.demux.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'demux' library on application startup.
    static {
        System.loadLibrary("demux");
    }

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Example of a call to a native method
        TextView tv = binding.sampleText;
        tv.setText(GetVersion());

        Button rtsp = (Button)findViewById(R.id.rtsp);
        rtsp.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View v)
            {
                Intent intent = new Intent(MainActivity.this,FFDemuxActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * A native method that is implemented by the 'demux' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    public native String GetVersion();

}